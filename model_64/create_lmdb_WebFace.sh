#!/usr/bin/env sh
# This script converts the deepface data into lmdb format.

# set -e

DATA=/data/Datasets/CASIA-maxpy-clean-crop-1/
DBTYPE=lmdb

echo "Creating $DBTYPE..."

rm -r ./WebFace_train_$DBTYPE 
rm -r ./WebFace_val_$DBTYPE

/home/ronghuaiyang/GitHub/caffe/build/tools/convert_imageset --shuffle \
--resize_height=82 --resize_width=82 \
$DATA train_data.txt  /data/Datasets/WebFace/train_size_82_$DBTYPE

echo "Create deepID_train_$DBTYPE done."

/home/ronghuaiyang/GitHub/caffe/build/tools/convert_imageset --shuffle \
--resize_height=82 --resize_width=82 \
$DATA val_data.txt  /data/Datasets/WebFace/val_size_82_$DBTYPE

echo "Create WebFace_val_$DBTYPE done."
echo "Computing image mean..."

/home/ronghuaiyang/GitHub/caffe/build/tools/compute_image_mean -backend=$DBTYPE \
/data/Datasets/WebFace/train_size_82_$DBTYPE /data/Datasets/WebFace/mean_size_82.binaryproto

echo "Done."
