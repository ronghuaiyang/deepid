# -*- coding: utf-8 -*-
import os
import caffe
# import face_verification
import re
import numpy as np
import matplotlib.pyplot as plt


def cosine_distance(x1, x2):
    return np.dot(x1, x2)/(np.linalg.norm(x1) * np.linalg.norm(x2))


def test_pairs(net, transformer, image_path, paris_list, p_n):
    fid = open(paris_list)
    pairs = fid.readlines()
    fid.close()
    distance = []

    for line in pairs:
        pair = re.split(' ', line)
        # print pair[0], pair[1]
        fe = []
        for line in pair:
            if line[-1] == '\n':
                line = line[:-1]
            file_path = os.path.join(image_path, line)
            im = caffe.io.load_image(file_path)
            # plt.imshow(im)
            # plt.show()
            net.blobs['data_1'].data[...] = transformer.preprocess('data_1', im)
            net.forward()
            fe.append(net.blobs['deepid_1'].data.flatten())

        # print fe[0].shape
        print 'fe_0: \n', fe[0]
        print 'fe_1: \n', fe[1]
        c_dis = cosine_distance(fe[0], fe[1])
        distance.append(c_dis)

        break

    
    # plt.plot(distance)
    # plt.show()
    #   return distance  


if __name__ == '__main__':
    solver = 'model_64/deepID_deploy.prototxt'
    model = 'model_64/deepID.caffemodel'
    # solver = 'example/deploy.prototxt'
    # model = 'example/snapshot_iter_750000.caffemodel'
    caffe.set_mode_gpu()
    net = caffe.Net(solver, model, caffe.TEST)

    # image preprocess
    # set data shape(1, 3, 64, 64)
    transformer = caffe.io.Transformer({'data_1': net.blobs['data_1'].data.shape})
    # set dim order （64，64，3）-> (3, 64, 64)
    transformer.set_transpose('data_1', (2, 0, 1))
    # substract mean
    # mean_value = [(‘B’, 102.9801), (‘G’, 115.9465), (‘R’, 122.7717)]
    mean_value = np.array([102.9801, 115.9465, 122.7717])
    transformer.set_mean('data_1', mean_value)
    # scale
    transformer.set_raw_scale('data_1', 1)
    # swap order rgb->bgr
    transformer.set_channel_swap('data_1', (2, 1, 0))

    net.blobs['data_1'].reshape(1, 3, 64, 64)

    image_path = '/data/Datasets/CASIA-maxpy-clean-crop-1'
    pair_list = 'positive_pair_list.txt'
    # test_pairs(net, transformer, image_path, pair_list, 1)
    pair_list = 'negitive_pair_list.txt'
    test_pairs(net, transformer, image_path, pair_list, 0)